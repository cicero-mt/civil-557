/*********************************************
 * OPL 12.10.0.0 Model
 * Author: Leandre
 * Creation Date: 19 juill. 2020 at 14:25:47
 *********************************************/

int st = ...;
range o = 1..st;
range d = 1..st;

{int} omega =...;
{int} phi=...;
{int} phi_prime= phi diff {1};

int Qtr = ...;
float C1 = ...;
float C2= ...;
int R=...;
int T=...;
range r = 1..R;
range t=1..T;
int G = ...;
int W = ...;

float L[o] = ...;
float U[o] = ...;
int D[o][d] = ...;

int M = sum(i in o,j in d)(D[i][j]);

dvar boolean X[o][t][r];
dvar boolean Z[o][t][r];
dvar int+ f[t][r];
dvar int+ v[o][d][t][r];
dvar int+ Po[o][t][r];
dvar int+ Pr[o][t][r];
dvar float+ c[t][r];
dvar float+ pax[t][r];

minimize sum(theta in t, rho in r, i in o)(W*(Po[i][theta][rho]+ Pr[i][theta][rho]));

subject to {	
	
	//First station is always a stop
	forall (rho in r, theta in t)
 	 	X[1][theta][rho] == 1;

	//At least one of the Shunting station other than 1 should be equal to 1
	forall (rho in r, theta in t)
		sum(i in phi_prime)(X[i][theta][rho]) >= 1;
		
	//Last station must be a shunting station (if intermediate station has an ulterior shunting station it can be a stop)
	forall (i in o, theta in t, rho in r)
	  		sum(k in phi_prime : k >= i)(X[k][theta][rho]) >= X[i][theta][rho];

	//Demand = People on board
	forall (i in o, j in o)
	    sum(rho in r, theta in t)(v[i][j][theta][rho]) == D[i][j];
	
	//No offer at stations where the train does not stop (impossible to get on/off the train)
	forall (i in o, theta in t, rho in r)
		sum(k in o)(v[i][k][theta][rho]) + sum(k in o)(v[k][i][theta][rho]) <= M * X[i][theta][rho];
	
	//Capacity (Q*f)
	forall (rho in r, theta in t, i in o) {
		sum(k in 1..i, j in i+1..st)(v[k][j][theta][rho]) <= Qtr * f[theta][rho];  //outbound
		sum(k in i..st, j in 1..i-1)(v[k][j][theta][rho]) <= Qtr * f[theta][rho];  //return
	}
	
	//P
	forall (i in o, rho in r, theta in t) {
		sum(k in 1..i-1, j in i+1..st)(v[k][j][theta][rho]) == Po[i][theta][rho]; //outbound
 		sum(k in 1..i-1, j in i+1..st)(v[j][k][theta][rho]) == Pr[i][theta][rho]; //return
	}
	
	//Z = Binary vector indicating the line last stop (shunting station)
	forall (i in o, rho in r, theta in t){
	 	Z[i][theta][rho] <= X[i][theta][rho];
	  	Z[i][theta][rho] <= 1 - (sum(k in o)(X[k][theta][rho]) - sum(k in o : k<=i)(X[k][theta][rho]))/st;
	}
	forall (rho in r, theta in t)
	  	sum(i in o) (Z[i][theta][rho]) == 1;
	
	//Cycle length to determine the number of trains thereafter (in minutes)
	forall (rho in r, theta in t)
	  c[theta][rho] >= 2 *(sum(i in o)(U[i]*Z[i][theta][rho]) + (G - 2*W) + 
	  					W*(sum(i in o)(X[i][theta][rho])));
	
	//Total passengers using the stop-schedule
	forall (rho in r, theta in t)
		pax[theta][rho] == sum(i in o,j in o)v[i][j][theta][rho];
	
}

