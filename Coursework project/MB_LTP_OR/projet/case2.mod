/*********************************************
 * OPL 12.10.0.0 Model
 * Author: Leandre
 * Creation Date: 19 juill. 2020 at 14:25:47
 *********************************************/

int st = ...;
range o = 1..st;
range d = 1..st;

{int} omega =...;
{int} phi=...;
{int} phi_prime= phi diff {1};

int Qtr = ...;
float C1 = ...;
float C2= ...;
int R=...;
int T=...;
range r = 1..R;
range t=1..T;
int G = ...;
int W = ...;
int M2 = 20; // (trains/h)
int M3 = 20; // (trains/h)

float L[o] = ...;
float U[o] = ...;
int D[o][d] = ...;

int M = sum(i in o,j in d)(D[i][j]);

dvar boolean X[o][t][r];
dvar boolean Z[o][t][r];
dvar float+ y[o][t][r];
dvar float+ w[o][t][r];
dvar int+ f[t][r]; //(trains/h)
dvar int+ v[o][d][t][r];
dvar int+ n[t][r];
dvar float K[t][r];
dvar float+ pax[t][r];

minimize C1 * sum(theta in t, rho in r)(n[theta][rho]) + C2 * sum(theta in t, rho in r)(K[theta][rho])  ;


subject to {	
	
	//Z = Binary vector indicating the line last stop (shunting station), y=auxiliary variable (f*Z),
	//w==auxiliary variable (f*X)
	forall (i in o, rho in r, theta in t){
	 	Z[i][theta][rho] <= X[i][theta][rho];
	  	Z[i][theta][rho] <= 1 - (sum(k in o)(X[k][theta][rho]) - sum(k in o : k<=i)(X[k][theta][rho]))/st;
		y[i][theta][rho] <= M2 * Z[i][theta][rho];
		y[i][theta][rho] <= f[theta][rho];
		y[i][theta][rho] >= f[theta][rho] - M2 *(1- Z[i][theta][rho]);
		w[i][theta][rho] <= M3 * X[i][theta][rho];
		w[i][theta][rho] <= f[theta][rho];
		w[i][theta][rho] >= f[theta][rho] - M3 *(1- X[i][theta][rho]);
	}
	forall (rho in r, theta in t)
	  	sum(i in o) (Z[i][theta][rho]) == 1;
	
	//Number of trains (using time in hours)
	forall (rho in r, theta in t)
	  n[theta][rho] >= 2/60*(
	  					sum(i in o)(U[i]*y[i][theta][rho]) + 
	  				  	(G - 2*W)*f[theta][rho] + 
	  				   	W*(sum(i in o)(w[i][theta][rho]))
	  				   	);

	//Total kilometers
	forall (rho in r, theta in t)	{
		K[theta][rho] >= sum(i in o)(L[i]*2*y[i][theta][rho]);
	}	
	//First station is always a stop
	forall (rho in r, theta in t)
 	 	X[1][theta][rho] == 1;

	//At least one of the Shunting station other than 1 should be equal to 1
	forall (rho in r, theta in t)
		sum(i in phi_prime)(X[i][theta][rho]) >= 1;
		
	//Last station must be a shunting station (if intermediate station has an ulterior shunting station it can be a stop)
	forall (i in o, theta in t, rho in r)
	  		sum(k in phi_prime : k >= i)(X[k][theta][rho]) >= X[i][theta][rho];

	//Demand = People on board
	forall (i in o, j in o)
	    sum(rho in r, theta in t)(v[i][j][theta][rho]) == D[i][j];
	
	//No offer at stations where the train does not stop (impossible to get on/off the train)
	forall (i in o, theta in t, rho in r)
		sum(k in o)(v[i][k][theta][rho]) + sum(k in o)(v[k][i][theta][rho]) <= M * X[i][theta][rho];
	
	//Capacity (Q*f)
	forall (rho in r, theta in t, i in o) {
		sum(k in 1..i, j in i+1..st)(v[k][j][theta][rho]) <= Qtr * f[theta][rho];  //outbound
		sum(k in i..st, j in 1..i-1)(v[k][j][theta][rho]) <= Qtr * f[theta][rho];  //return
	}
	
	//Total passengers using the stop-schedule
	forall (rho in r, theta in t)
		pax[theta][rho] == sum(i in o,j in o)v[i][j][theta][rho];
}

