import pandas as pd

df = pd.read_csv("predictions_automl.csv", index_col="trip_id")

df = df[["choice_drive_score", "choice_passenger_score", "choice_walk_score", "choice_pt_score", "choice_cycle_score"]]

df.rename(columns={'trip_id':'id',
                    'choice_drive_score':'drive',
                    'choice_passenger_score':'passenger',
                    'choice_walk_score': 'walk',
                    'choice_pt_score': 'pt',
                    'choice_cycle_score': 'cycle'}, 
                 inplace=True)

df.to_csv('predictions_automl.csv')