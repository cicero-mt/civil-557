{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Categorical features with Decision Tree Classifier\n",
    "\n",
    "This notebook was written by Gael Lederrey and Tim Hillel (tim.hillel@epfl.ch) for the Decision-aid methodologies in transportation course at EPFL (http://edu.epfl.ch/coursebook/en/decision-aid-methodologies-in-transportation-CIVIL-557).\n",
    "\n",
    "Please contact before distributing or reusing the material below.\n",
    "\n",
    "## Overview\n",
    "\n",
    "In this notebook, we will fit and investigate a Decision Tree Classifier using the full dataset (including the categorical variables) in order to predict the travel mode taken.\n",
    "\n",
    "The notebook will cover:\n",
    "1. Numerical and binary encoding of categorical variables\n",
    "2. External validation and train-validate-test\n",
    "3. Use of the Decision Tree Classifier\n",
    "\n",
    "## Set-up"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import data, and set index to trip_id column\n",
    "\n",
    "df = pd.read_csv('data/dataset.csv', index_col='trip_id')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data processing\n",
    "\n",
    "As discussed in the lecture, we will be working with all of the features in the dataset, including categorical features. \n",
    "\n",
    "Use the `dtypes` method on the dataframe `df` to investigate which features are likely to be categorical. \n",
    "\n",
    "You can look at those columns to see if your guess is correct (remember the `head` method!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Numerical encoding\n",
    "\n",
    "One of the categorical values is our ***target***, `travel_mode`. \n",
    "\n",
    "As such, for this column we want to use *linear encoding*. \n",
    "\n",
    "Using a dictionary, and the `replace` method in Pandas, encode the `travel_mode` column numerically, so that *walk* is 0, *cycle* is 1, *pt* is 2, and *drive* is 3. \n",
    "\n",
    "*Hint*: Look at the `inplace` argument for the `replace` method!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Binary encoding\n",
    "\n",
    "Now we will use *binary encoding* (also known as *one-hot encoding* to turn each of the three remaining categorical features into $n$ binary columns. \n",
    "\n",
    "Use a *colon* (:) as the separator for each categorical value (e.g. `faretype:full`), and store the finished dataframe (including all features) as `df_processed`.\n",
    "\n",
    "*Hint: Look at the pandas **function** `get_dummies`*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "categorical_cols = ['purpose', 'fueltype', 'faretype']\n",
    "\n",
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train-validate-test and external validation\n",
    "\n",
    "In order to create a test set which represents *external validation*, we will separate out the first two years of data  from the final (third) year of data. \n",
    "\n",
    "The first two years are considered as the *training/validation* set. The final test year is used as the unseen *holdout test* set for your finished model.\n",
    "\n",
    "Separate the data into two separate dataframes:\n",
    "* `train_validate` for `survey_year == < 3`\n",
    "* `test` for `survey_year == 3`\n",
    "\n",
    "Save these as `train_validate.csv` and `test.csv` respectively (remember to store them inside the data folder)!\n",
    "\n",
    "*Hint: remember to use `df_processed` and not `df`!*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset contains several *ID* and *context* columns, which are not useful as *features* to predict mode choice. As such, these columns should not be included in the model input. \n",
    "\n",
    "The following code creates two new dataframes, `y` (the target), and `X` (features). \n",
    "\n",
    "Check you understand how it works (including the list comprehension!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target = ['travel_mode']\n",
    "id_context = ['trip_id', \n",
    "              'household_id', \n",
    "              'person_n', \n",
    "              'trip_n',\n",
    "              'survey_year',\n",
    "              'travel_year'\n",
    "             ]\n",
    "features = [c for c in train_validate.columns \n",
    "            if c not in (target + id_context)]\n",
    "\n",
    "y = train_validate[target]\n",
    "X = train_validate[features]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Train-validate split"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now need to separate the `train_validate` data into separate `train` and `validate` sets. \n",
    "\n",
    "This time, we can use the function `train_test_split` from `sklearn.model_selection` to split our dataset into a train and validate (test) set.\n",
    "\n",
    "Use a `test_size` of 0.2, and a `random_state` of 42. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use and test Decision Tree\n",
    "\n",
    "We will be using the `DecisionTreeClassifier` from the `tree` module in *scikit-learn*, as well as `accuracy score`, `precision_recall_fscore_support`, and `confusion_matrix` from the `metrics` module. \n",
    "\n",
    "#### Import libraries\n",
    "\n",
    "Import the relevant functions/classes here. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To assess the performance of our model, we can create a function that will give us the accuracy, precision, and recall together. Check you can follow how the function works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classes = np.array(['walk', 'cycle', 'pt', 'drive'])\n",
    "\n",
    "def acc_pre_rec(y_true, y_pred, verbose=False):\n",
    "    ''' Returns accuracy, precision, and recall together. \n",
    "    If verbose is set to True, it prints the scores for \n",
    "    each mode.'''\n",
    "    acc = accuracy_score(y_true, y_pred)\n",
    "    prec, rec, fsc, sup = precision_recall_fscore_support(\n",
    "        y_true, y_pred)\n",
    "    \n",
    "    if verbose:\n",
    "        print(\"Accuracy: \\n    {:.3f}%\".format(acc*100))\n",
    "        scrs = ['Precision', 'Recall']\n",
    "        for i, scr in enumerate([prec, rec]):\n",
    "            str_ = '%;\\n    '.join(\n",
    "                \"{} - {:.3f}\".format(classes[i], 100*s) \n",
    "                for i, s in enumerate(scr)\n",
    "            )\n",
    "            print(\"{}: \\n    {}%\".format(scrs[i], str_))\n",
    "    else:\n",
    "        return acc, prec, rec\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using DTC\n",
    "\n",
    "First, create an instance of the *decision tree classifier* and fit it to the training data. Use default values for all hyperparameters.\n",
    "\n",
    "Call the classifier `dtc`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, use the fitted model to predict the validation data. Call the predictions `y_pred`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter your code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the results. What can you see from the precision and recall? Which modes are most/least well represented in the results?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "acc_pre_rec(y_validate, y_pred, True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization\n",
    "\n",
    "We can visualise the tree using the `graphviz` library.\n",
    "\n",
    "You can install graphviz and pydotplus on your computer by opening the terminal and typing\n",
    "\n",
    "    conda install graphviz python-graphviz pydotplus\n",
    "    \n",
    "Do not worry if you cannot get `graphviz` working, below is an example output for `max_depth = 3`.\n",
    "\n",
    "<img src=\"data/tree_md3.png\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image  \n",
    "from sklearn.tree import export_graphviz\n",
    "import pydotplus\n",
    "from graphviz import Source"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = DecisionTreeClassifier(max_depth=1, criterion=\"gini\")\n",
    "clf = clf.fit(X_train, y_train)\n",
    "\n",
    "dot_data = export_graphviz(clf, out_file=None, \n",
    "                filled=True, rounded=True,\n",
    "                special_characters=True,\n",
    "                feature_names = X_train.columns,\n",
    "                class_names = classes)\n",
    "graph = pydotplus.graph_from_dot_data(dot_data)\n",
    "img = Image(graph.create_png())\n",
    "display(img)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's change the criterion for measuring the quality of the split and use the entropy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = DecisionTreeClassifier(max_depth=1, criterion=\"entropy\")\n",
    "clf = clf.fit(X_train, y_train)\n",
    "\n",
    "dot_data = export_graphviz(clf, out_file=None,  \n",
    "                filled=True, rounded=True,\n",
    "                special_characters=True,\n",
    "                feature_names = X_train.columns,\n",
    "                class_names = classes)\n",
    "graph = pydotplus.graph_from_dot_data(dot_data)  \n",
    "img = Image(graph.create_png())\n",
    "display(img)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***What is happening?***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Viewing the confusion matrix\n",
    "\n",
    "It can be sometimes difficult to assess the performance the model by simply using the accuracy, precision, and recall. Thus, it is often better to consider the *confusion matrix* as a whole. The function `plot_confusion_matrix` from *scikit-learn*'s `metrics` module plots a nicely formatted confusion matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the confusion matrix for our original classifier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import confusion_matrix, plot_confusion_matrix\n",
    "\n",
    "print('Confusion matrix, without normalization:')\n",
    "display(confusion_matrix(y_validate, y_pred))\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "plot_confusion_matrix(dtc, X_validate, y_validate, display_labels= classes, values_format= 'd', cmap=plt.cm.Blues, ax = ax)\n",
    "ax.set_title('Confusion matrix for DT')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "acc_pre_rec(y_validate, y_pred, verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modifying hyperparameters\n",
    "\n",
    "Try changing the default hyperparameters for the DTC. What happens? Can you improve the accuracy score on the validation set?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing the model\n",
    "\n",
    "Now that we have finalised our model specification, we can fit it on **ALL** of the train/validate data, and test it on the holdout test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dtc.fit(X, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_test = test[target]\n",
    "X_test = test[features]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_test_pred = dtc.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print('Confusion matrix, without normalization:')\n",
    "display(confusion_matrix(y_test, y_test_pred))\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "plot_confusion_matrix(dtc,X_test, y_test, display_labels=classes, values_format= 'd', cmap=plt.cm.Blues, ax = ax)\n",
    "ax.set_title('Confusion matrix for DT')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "acc_pre_rec(y_test, y_test_pred, verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***What has happened? Why is the performance so much lower?***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also look at the **train** fit to give us a clue!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Confusion matrix, without normalization:')\n",
    "display(confusion_matrix(y, dtc.predict(X)))\n",
    "\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "plot_confusion_matrix(dtc, X, y, display_labels=classes,values_format= 'd', cmap=plt.cm.Blues, ax = ax)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
